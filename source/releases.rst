.. title:: Releases

===============
Making releases
===============

These instructions are for developer making release of FEniCS
packages.


Releasing packages
==================

Check that all CI systems are green before making a release.


Python packages
---------------

These instruction cover:

- FFC
- FIAT
- UFL
- dijitso


Making the release
^^^^^^^^^^^^^^^^^^

#. Checkout branch and make sure it is clean::

     git checkout master
     git clean -fdx

#. Update release notes.

#. Update version number in ``<src>/__init__.py``

#. Commit changes.

#. Tag repository and push tag::

     git tag -a $VERSION -m "Version $VERSION"
     git push origin $BRANCH

#. Update version number and add ``dev`` in
   ``<src>/__init__.py``. Commit and push.


Uploading to PyPI
^^^^^^^^^^^^^^^^^^

This applies to packages FFC, dijitso, FIAT, UFL
and the FEniCS Project Python Metapackage.

This should be done soon after a release is made.

#. Checkout release tag and make sure it is clean::

     git checkout tags/$VERSION
     git clean -fdx

#. Build source distribution::

     python3 setup.py sdist

#. Sign the package::

     gpg --detach-sign -a dist/package-1.0.1.tar.gz

   The gpg key is in the Steering Council LastPass account.

#. Upload to PyPI::

     twine upload dist/package-1.0.1.tar.gz dist/package-1.0.1.tar.gz.asc

   The username on PyPI is ``fenicsproject``.

.. note::

   To use the PyPI test repository::

     twine upload --repository-url=https://test.pypi.org/legacy/ dist/*

.. todo::

   - Update on Read-the-Docs


DOLFIN
------

.. todo::

   Fill in

To create tarball from Git tag::

    VERSION=2018.1.0.post0
    git archive -9 --prefix=dolfin-${VERSION}/ -o dolfin-${VERSION}.tar.gz ${VERSION}

Don't forget to make sure that Git LFS data files are packaged.
Then sign the tarball using FEniCS PGP keypair by procedure
above and upload both files to Bitbucket.


Docker containers
=================

.. todo::

   Fill in


Anaconda packages
=================

.. todo::

   Fill in


Ubuntu PPA
==========

.. todo::

   Fill in


Versioning scheme
=================

FEniCS uses a hybrid date-based/serial version numbering
scheme. Releases have a version number::

    year.number.fix
    
In the case of packaging errors (rather than bugs in the 
release) you can also introduce .postx releases, e.g.::

    year.number.fix.post0
    
FFC and the FEniCS Python Metapackage
=====================================

Assume we want to release the ``2017.2.0`` release. ``master``
would currently specify::

    VERSION = "2017.2.0.dev0"
    RESTRICT_REQUIREMENT = ">=2017.2.0.dev0,<2017.3"

1. On the release branch ``2017.2.0``::

    VERSION = "2017.2.0"
    RESTRICT_REQUIREMENTS = ">=2017.2,<2017.3"

The upper bound should be `tight` against the current 
release, i.e. don't do::

    VERSION = "2017.2.0"
    RESTRICT_REQUIREMENTS = ">=2017.2,<2018.1"
    
2. The update to ``master`` post-release would be::

    VERSION = "2018.1.0.dev0"
    RESTRICT_REQUIREMENTS = ">=2018.1.0.dev0,<2018.2"

Other FEniCS Python packages
============================

1. For the release branch, remove the ``.dev0`` suffix::

    VERSION = "year.number.0"

2. On release of a new version, the ``VERSION`` string in ``setup.py``
on ``master`` branches of the FEniCS Python packages 
should be incremented::

    VERSION = "year.number+1.0.dev0"
    
In the case of a release late in the year, it may be
more appropriate to increment the year::

    VERSION = "year+1.number.0.dev0"

.. todo::

   Document and explain
