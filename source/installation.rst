.. title:: Installation

============
Installation
============

This guide summarises how to install FEniCS. The most reliable way to
get started is using the Docker containers (Linux, macOS and Windows).

For installation in high performance computing clusters we recommend
always building from source.


From source
===========

FEniCS consists of Python components
`FIAT    <https://github.com/FEniCS/fiat>`_,
`dijitso <https://bitbucket.org/fenics-project/dijitso>`_,
`UFL     <https://github.com/FEniCS/ufl>`_,
`FFC     <https://bitbucket.org/fenics-project/ffc>`_,
and C++/Python components
`DOLFIN  <https://bitbucket.org/fenics-project/dolfin>`_
and optional
`mshr    <https://bitbucket.org/fenics-project/mshr>`_.
DOLFIN is the main user interface of FEniCS, both for C++ and Python.

FEniCS needs Python 3. For building `CMake <https://cmake.org/>`_ is
needed and `pip <https://pypi.org/project/pip/>`_ is recommended.

For building optional Python interface of DOLFIN and mshr, `pybind11
<https://pybind11.readthedocs.io>`_ is needed since version ``2018.1.0``.
To install it::

   PYBIND11_VERSION=2.2.3
   wget -nc --quiet https://github.com/pybind/pybind11/archive/v${PYBIND11_VERSION}.tar.gz
   tar -xf v${PYBIND11_VERSION}.tar.gz && cd pybind11-${PYBIND11_VERSION}
   mkdir build && cd build && cmake -DPYBIND11_TEST=off .. && make install

It may be useful to add ``cmake`` flag ``-DCMAKE_INSTALL_PREFIX=<prefix>``
to install to a user location.


Stable version
--------------

To install the Python components of FEniCS::

  pip3 install fenics-ffc --upgrade

This will install FFC and its dependencies. It may be useful to add flag
``--user`` or ``--prefix=<prefix>`` to install to a user location.
To install DOLFIN, and optionally mshr and/or Python interface of DOLFIN/mshr::

  FENICS_VERSION=$(python3 -c"import ffc; print(ffc.__version__)")
  git clone --branch=$FENICS_VERSION https://bitbucket.org/fenics-project/dolfin
  git clone --branch=$FENICS_VERSION https://bitbucket.org/fenics-project/mshr
  mkdir dolfin/build && cd dolfin/build && cmake .. && make install && cd ../..
  mkdir mshr/build   && cd mshr/build   && cmake .. && make install && cd ../..
  cd dolfin/python && pip3 install . && cd ../..
  cd mshr/python   && pip3 install . && cd ../..

It may be useful to add ``cmake`` flag ``-DCMAKE_INSTALL_PREFIX=<prefix>``
and ``pip3`` flag ``--user`` or ``--prefix=<prefix>`` to install to a user
location.

See detailed instructions for `DOLFIN
<https://fenicsproject.org/docs/dolfin/dev/python/installation.html>`_
and `mshr <https://bitbucket.org/fenics-project/mshr/wiki/Installation>`_.

.. todo::

  Update DOLFIN documentation for latest release, including updates for
  building Python interface with pybind11, and provide a link to
  documentation of stable version here.

  Consolidate multiple documentation locations:
  `RTD <https://fenics-dolfin.readthedocs.io/en/latest/>`_,
  `webserver <https://fenicsproject.org/docs/dolfin/>`_.


Development version
-------------------

The FEniCS source code can be found on the `FEniCS Bitbucket pages
<https://bitbucket.org/fenics-project/>`_ and the `FEniCS Github pages <https://github.com/FEniCS/>`_. To download and build current
development version run the following commands::

  git clone https://github.com/FEniCS/fiat.git
  git clone https://bitbucket.org/fenics-project/dijitso
  git clone https://github.com/FEniCS/ufl.git
  git clone https://bitbucket.org/fenics-project/ffc
  git clone https://bitbucket.org/fenics-project/dolfin
  git clone https://bitbucket.org/fenics-project/mshr
  cd fiat    && pip3 install . && cd ..
  cd dijitso && pip3 install . && cd ..
  cd ufl     && pip3 install . && cd ..
  cd ffc     && pip3 install . && cd ..
  mkdir dolfin/build && cd dolfin/build && cmake .. && make install && cd ../..
  mkdir mshr/build   && cd mshr/build   && cmake .. && make install && cd ../..
  cd dolfin/python && pip3 install . && cd ../..
  cd mshr/python   && pip3 install . && cd ../..

See detailed instructions for `DOLFIN
<https://fenicsproject.org/docs/dolfin/dev/python/installation.html>`_
and `mshr <https://bitbucket.org/fenics-project/mshr/wiki/Installation>`_.


Debian/Ubuntu packages
======================

FEniCS is available as a package for Debian and Ubuntu [1]_ in the
official repositories. If you are using Ubuntu, we recommend the
Ubuntu PPA.

Ubuntu PPA
----------

The Ubuntu Personal Package Archives (PPA) version is the latest
release of FEniCS. To install `FEniCS from the Ubuntu PPA
<https://launchpad.net/~fenics-packages/+archive/ubuntu/fenics>`_:

.. code-block:: bash

    sudo apt-get install --no-install-recommends software-properties-common
    sudo add-apt-repository ppa:fenics-packages/fenics
    sudo apt-get update
    sudo apt-get install --no-install-recommends fenics


Official Debian/Ubuntu repositories
-----------------------------------

The version of FEniCS in the Debian/Ubuntu repositories [1]_ is not
always the most recent FEniCS release. To install FEniCS from the
official Debian/Ubuntu repositories:

.. code-block:: bash

    sudo apt-get update
    sudo apt-get install --no-install-recommends fenics

.. [1] mshr is not available from official Debian and Ubuntu
       repositories.


Containers/Docker (Linux, macOS and Windows - 64 bit)
=====================================================

A collection of Docker containers for FEniCS are available.  To get
started, install `Docker <https://www.docker.com/>`_, and then run

.. code-block:: bash

   docker run -ti -v $(pwd):/home/fenics/shared -w /home/fenics/shared quay.io/fenicsproject/stable:current

A helper script is also available. To install it automatically
to ``$HOME/.local/bin``, run the command:

.. code-block:: bash

   curl -s https://get.fenicsproject.org | bash

To run the FEniCS Docker image, use the command ``fenicsproject run``. For
more options and features, see ``fenicsproject help``.

For detailed instruction on the Docker containers and background, a
see http://fenics-containers.readthedocs.org/en/latest/ for how to run
FEniCS inside a container.


Conda (Linux and macOS - 64 bit)
================================

To install the latest FEniCS release from using `conda
<http://conda.pydata.org/>`_::

  conda install -c conda-forge fenics

To install a development snapshot::

   conda install -c conda-forge/label/prerelease -c conda-forge fenics

The packages are part of `conda forge
<https://conda-forge.github.io/>`_ (see
https://anaconda.org/conda-forge/fenics), and the recipes are
maintained at https://github.com/conda-forge/fenics-feedstock/.

.. note::

   Conda support is experimental and subject to changes.
